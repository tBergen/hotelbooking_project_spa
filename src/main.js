import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import jquery from 'jquery'
// import popperjs from 'popper.js'
// import bootstrap from 'bootstrap'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
