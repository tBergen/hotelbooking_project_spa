export default class Hotel {
	constructor() {
		this.name = '';
	}

	static fromJSON(json) {
		const self = new Hotel();

		self.name = json.name;

		return self;
	}

	
}